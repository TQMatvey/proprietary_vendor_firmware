FIRMWARE_IMAGES := \
    abl \
    aop \
    bluetooth \
    cmnlib \
    cmnlib64 \
    devcfg \
    dsp \
    hyp \
    imagefv \
    keymaster \
    LOGO \
    modem \
    multiimgoem \
    qupfw \
    storsec \
    tz \
    uefisecapp \
    xbl \
    xbl_config

AB_OTA_PARTITIONS += $(FIRMWARE_IMAGES)
